# Description
#   Throws tantrums. Cleans up afterwards.
#
# Dependencies:
#   None
#
# Commands:
#   hubot (flip|throw) <something> - Freak out!
#
# Author:
#   zobar (David P. Kleinschmidt)
#

module.exports = (robot) ->
  chars = '!¡ "„ &⅋ \', () .˙ 1Ɩ 2ᄅ 3Ɛ 4ㄣ 5ϛ 69 7ㄥ <> ?¿ A∀ Bq CƆ Dp EƎ FℲ
           Gפ Jſ Kʞ L˥ MW PԀ Rɹ T┴ U∩ VΛ Y⅄ [] _‾ `, aɐ bq cɔ dp eǝ fɟ gƃ hɥ
           iı jɾ kʞ mɯ nu rɹ tʇ vʌ wʍ yʎ {} ︵︶ ⊏⊐ ⊂⊃ ┻┳'

  words =
    '(╯°□°)╯':    '╭(｡˘｡╭)',
    '╭(｡˘｡╭)':    '(╯°□°)╯',
    'ノ( º _ ºノ)': '(╮｡˘｡)╮',
    '(╮｡˘｡)╮':    'ノ( º _ ºノ)',
    '.⁎*⁑⁑*':     '*⁑⁑*⁎.',
    '*⁑⁑*⁎.':     '.⁎*⁑⁑*',
    bottle:       '⊏⊃-',
    confetti:     '.⁎*⁑⁑*',
    self:         '╭(｡˘｡╭)',
    table:        '┻━┻'

  inversions = do ->
    vice_versa = (map, [first, second]) ->
      [map[first], map[second]] = [second, first]
      map

    (chars.split /\s+/).reduce vice_versa, words

  invertible = do ->
    regexpEscape = (string) -> string.replace /[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&'
    escaped = (regexpEscape string for string of inversions)

    new RegExp "^(.*?)(.|#{escaped.join '|'})?$"

  invert = (input) -> switch input
    when '' then ''
    else
      [_, tail, head] = input.match invertible
      head = inversions[head] ? head
      head + invert tail

  robot.respond /(?:throw|flip)\s+([^\s].*?)\s*$/i, (msg) ->
    inverted = invert msg.match[1]
    msg.send "(╯°□°)╯︵ #{inverted}"
    setTimeout (-> msg.send "#{invert inverted} ノ( º _ ºノ)"), 7500
